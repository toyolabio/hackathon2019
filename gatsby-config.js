module.exports = {
  siteMetadata: {
    title: 'ええじゃないか学生ハッカソンin豊橋',
    author: '@toyolabio',
    siteUrl: 'https://hack19.toyolab.io',
    social: {
      twitter: '@toyolabio',
    },
    description:
      '本イベントは、豊橋のまちが抱える課題について、学生の目線でアイデアを考案し、オープンデータを用いた手段で具体化することを目的としたハッカソン（製作会）イベントです。',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Hack19 Toyolab',
        short_name: 'Hack19 Toyolab',
        start_url: '/',
        background_color: '#b24429',
        theme_color: '#b24429',
        display: 'minimal-ui',
        icon: 'src/assets/images/favicon.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline',
  ],
}
