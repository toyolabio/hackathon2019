import React, { useState } from 'react'

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

const Contact = props => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')

  const handleSubmit = e => {
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({ 'form-name': 'contact', name, email, message }),
    })
      .then(() => alert('お問い合わせを送信しました。'))
      .catch(error => alert(error))

    e.preventDefault()
  }
  const handleReset = e => {
    setName('')
    setEmail('')
    setMessage('')
    e.preventDefault()
  }

  const handleChange = e => {
    const { name, value } = e.target
    switch (name) {
      case 'name':
        setName(value)
        break
      case 'email':
        setEmail(value)
        break
      case 'message':
        setMessage(value)
        break
      default:
        break
    }
  }

  return (
    <section id="contact">
      <div className="inner">
        <section>
          <header className="major">
            <h3>お問い合わせ</h3>
          </header>
          <form name="contect" onSubmit={handleSubmit} onReset={handleReset}>
            <div className="field half first">
              <label htmlFor="name">お名前</label>
              <input
                type="text"
                name="name"
                value={name}
                onChange={handleChange}
                id="name"
              />
            </div>
            <div className="field half">
              <label htmlFor="email">メール</label>
              <input
                type="email"
                name="email"
                value={email}
                onChange={handleChange}
                id="email"
              />
            </div>
            <div className="field">
              <label htmlFor="message">お問い合わせ内容</label>
              <textarea
                name="message"
                value={message}
                onChange={handleChange}
                id="message"
                rows="6"
              ></textarea>
            </div>
            <ul className="actions">
              <li>
                <input type="submit" value="送信" className="special" />
              </li>
              <li>
                <input type="reset" value="クリア" />
              </li>
            </ul>
          </form>
        </section>
        <section className="split">
          <section>
            <div className="contact-method">
              <span className="icon solid alt fa-users"></span>
              <h3>運営団体</h3>
              <span>
                2019年4月に設立された学生任意団体 「Toyohashi Technology
                Laboratory（通称：とよラボ）」
                の代表である東昭太朗が発起人となり、団体として豊橋市のオープンデータワークショップ等開催補助金へ申請、採択を受ける。その後、とよラボに参加するメンバーを中心に団体内外からハッカソン運営メンバーを募集。「ええじゃないか学生ハッカソン実行委員会」を発足させ、実行委員長に豊島秀典を選出し、12名のメンバーとともに開催に向けた活動を行っている。
              </span>
            </div>
          </section>
          <section>
            <div className="contact-method">
              <span className="icon solid alt fa-envelope"></span>
              <h3>Email</h3>
              <p>conf@toyolab.io</p>
            </div>
          </section>
        </section>
      </div>
    </section>
  )
}

export default Contact
