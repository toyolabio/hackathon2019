import React, { useState, useEffect } from 'react'

import '../assets/scss/main.scss'
import Header from './Header'
import Contact from './Contact'
import Footer from './Footer'

const Layout = ({ children }) => {
  const [loading, setLoading] = useState('is-loading')

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading('')
    }, 100)
    return () => clearTimeout(timer)
  }, [])

  return (
    <div className={`body ${loading}`}>
      <div id="wrapper">
        <Header />
        {children}
        <Contact />
        <Footer />
      </div>
    </div>
  )
}

export default Layout
