import React from 'react'

const Banner = props => (
  <section id="banner" className="major">
    <div className="inner">
      <header className="major">
        <h1>
          ええじゃないか
          <br />
          学生ハッカソンin豊橋
        </h1>
        <h2>学生による豊かなまちづくり</h2>
      </header>
      <div className="content">
        <p>
          　本イベントは、豊橋のまちが抱える課題について、学生の目線でアイデアを考案し、オープンデータを用いた手段で具体化することを目的としたハッカソン（製作会）イベントです。
        </p>
      </div>
      <div className="table-wrapper">
        <table className="alt info-table">
          <tbody>
            <tr>
              <td>日程</td>
              <td>
                2019年12月26日(木)
                <br />
                2019年12月27日(金)
              </td>
            </tr>
            <tr>
              <td>会場</td>
              <td>
                MUSASHi Innovation Lab CLUE
                <br />
                豊橋市駅前大通一丁目135
                <br />
                COCOLA AVENUE 3F
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <ul className="actions center">
        <li>
          <a href="#one" className="button next scrolly">
            詳しい情報
          </a>
        </li>
      </ul>
    </div>
  </section>
)

export default Banner
