import React from 'react'
import Layout from '../components/layout'
import Banner from '../components/Banner'

import pic08 from '../assets/images/pic08.jpg'
import pic09 from '../assets/images/pic09.jpg'
import pic10 from '../assets/images/pic10.jpg'
import SEO from '../components/Seo'

class HomeIndex extends React.Component {
  render() {
    return (
      <Layout>
        <SEO />
        <Banner />

        <div id="main">
          <section id="one">
            <div className="inner">
              <header className="major">
                <h2>はじめに</h2>
              </header>
              <p>TODO文章を追加</p>
            </div>
          </section>

          <section id="two" className="spotlights">
            <section>
              <span className="image">
                <img src={pic08} alt="" />
              </span>
              <div className="content">
                <div className="inner">
                  <header className="major">
                    <h3>ハッカソン</h3>
                  </header>
                  <p>TODO文章を追加</p>
                </div>
              </div>
            </section>

            <section>
              <span className="image">
                <img src={pic09} alt="" />
              </span>
              <div className="content">
                <div className="inner">
                  <header className="major">
                    <h3>オープンデータ</h3>
                  </header>
                  <p>TODO文章を追加</p>
                </div>
              </div>
            </section>

            <section>
              <span className="image">
                <img src={pic10} alt="" />
              </span>
              <div className="content">
                <div className="inner">
                  <header className="major">
                    <h3>学生エンジニアとの交流</h3>
                  </header>
                  <p>TODO文章を追加</p>
                </div>
              </div>
            </section>
          </section>

          <section id="three">
            <div className="inner">
              <header className="major">
                <h2>スケジュール</h2>
              </header>
              <p>TODO文章を追加</p>
            </div>
          </section>

          <section id="four">
            <div className="inner">
              <header className="major">
                <h2>応募</h2>
              </header>
              <p>TODO文章を追加</p>
              <ul className="actions">
                <li>
                  <a
                    href="https://toyolab.connpass.com/event/151713/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="button"
                  >
                    応募する
                  </a>
                </li>
              </ul>
            </div>
          </section>

          <section id="five">
            <div className="inner">
              <header className="major">
                <h2>開催概要</h2>
              </header>
              <div className="table-wrapper">
                <table>
                  <tbody>
                    <tr>
                      <td>テーマ</td>
                      <td>豊橋をオープンデータで盛り上げる</td>
                    </tr>
                    <tr>
                      <td>日程</td>
                      <td>2019年12月26日(木) - 12月27日(金)</td>
                    </tr>
                    <tr>
                      <td>会場</td>
                      <td>
                        MUSASHi Innovation Lab CLUE 豊橋市駅前大通一丁目135
                        COCOLA AVENUE 3F
                      </td>
                    </tr>
                    <tr>
                      <td>募集人数</td>
                      <td>20〜30人</td>
                    </tr>
                    <tr>
                      <td>参加資格</td>
                      <td>
                        ・まちが抱える課題解決に興味のある学生
                        <br />
                        ・オープンデータに興味のあるエンジニア・デザイナー志望の学生
                      </td>
                    </tr>
                    <tr>
                      <td>参加費</td>
                      <td>無料</td>
                    </tr>
                    <tr>
                      <td>connpass</td>
                      <td>
                        <a
                          href="https://toyolab.connpass.com/event/151713/"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          https://toyolab.connpass.com/event/151713/
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td>参加</td>
                      <td>
                        <a
                          href="https://toyolab.connpass.com/event/151713/"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          connpass
                        </a>
                        からお願いします。
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <ul className="actions center">
                <li>
                  <a
                    href="https://toyolab.connpass.com/event/151713/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="button"
                  >
                    応募する
                  </a>
                </li>
              </ul>
            </div>
          </section>

          <section id="six">
            <div className="inner">
              <header className="major">
                <h2>会場</h2>
              </header>
              <div className="embed-container">
                <iframe
                  title="gmap"
                  width="600"
                  height="500"
                  id="gmap_canvas"
                  src="https://maps.google.com/maps?q=%E8%B1%8A%E6%A9%8B%E5%B8%82%E9%A7%85%E5%89%8D%E5%A4%A7%E9%80%9A%E4%B8%80%E4%B8%81%E7%9B%AE135%20COCOLA%20AVENUE%203F&t=&z=17&ie=UTF8&iwloc=&output=embed"
                  frameBorder="0"
                  scrolling="no"
                  marginHeight="0"
                  marginWidth="0"
                ></iframe>
              </div>
              <p></p>
              <ul className="actions center">
                <li>
                  <a
                    href="http://www.musashi.co.jp/clue/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="button"
                  >
                    会場のウェブサイト
                  </a>
                </li>
              </ul>
            </div>
          </section>
        </div>
      </Layout>
    )
  }
}

export default HomeIndex
